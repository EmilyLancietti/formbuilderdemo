# FormioDemo

Simple Demo implementing a simple Form Builder Page using the OpenSource version of [<form.io>](https://www.form.io).


## Install

    docker-compose build
    docker-compose up

## <form.io> API Platform

Navigate to [http://localhost:8080](http://localhost:8080) and log in with the following credentials to have access to the <form.io> API Platform: 

    
|       Field    	        |    Value          |
| ------------------------- | ----------------- |
| Email   	      	        | admin@example.com |
| Password			        |  admin.123        |

## Angular Live Server

Navigate to [http://localhost:4200/](http://localhost:4200/) to explore the Demo.

Log in using the previous credentials to have access to the form management section.

## Environment

A template.env file is provided inside the project folder. Necessary Environment variables are:

* MONGO: mongo db used for storing form.io forms and resources
* DB_URL: mongo db used for storing custom forms created inside the demo application

## DB Dump

    mongodump --db [database_name] --out [output_directory]
    mongodump --host [database_host] --port [database_port] --out [output_directory]
    
Docs: [mongodump](https://docs.mongodb.com/manual/reference/program/mongodump/)

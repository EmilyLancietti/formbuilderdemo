const express = require('express');
const bodyParser = require('body-parser');
const models = require('../models/form');
var response = require('../modules/json_response');

const router = express.Router();

router.use(bodyParser.json());

router.route('/')
// listConsents: list consents registered
    .get(function (req, res) {
        models.Forms
            .find({})
            .sort([['name', 1]])
            .then(function (results) {
                response(res, "OK", {data: results}, 200);
            })
            .catch(function (err) {
                response(res, "Invalid request", req.originalUrl, 400);
            });
    })
    // createConsent: create a consent
    .post(function (req, res) {
        if (req.body.id) {
            // Update form
            models.Forms
                .findById(req.body.id)
                .then(function (form) {
                    // Check if form changed
                    if (JSON.stringify(form.form) !== JSON.stringify(req.body.form)) {
                        // Form changed
                        models.Forms
                            .create({
                                name: req.body.name,
                                description: req.body.description,
                                version: form.version + 1,
                                form: req.body.form
                            })
                            .then(function (form) {
                                response(res, 'Created', form, 201);
                            })
                            .catch(function (error) {
                                // Duplicate (name, version)
                                if (error.code === 11000) {
                                    response(res, `Invalid request: (${form.name}, ${form.version}) already exists`, req.body, 400);
                                } else {
                                    response(res, 'Invalid request', error, 400);
                                }
                            })
                    } else {
                        // Form not changed
                        form.name = req.body.name;
                        form.description = req.body.description;

                        form
                            .save(function (error) {
                                if (error) {
                                    response(res, 'Invalid request', error, 400);
                                } else {
                                    response(res, 'Updated', form, 200);
                                }
                            })
                    }
                })
                .catch(function (error) {
                    response(res, 'Invalid id', error, 400);
                })
        } else {
            // Create new form
            models.Forms.create({
                name: req.body.name,
                description: req.body.description,
                form: req.body.form
            }).then(function (form) {
                response(res, 'Created', form, 201);
            }).catch(function (error) {
                response(res, 'Invalid request', error, 400);
            })
        }
    })
    .delete(function (req, res) {
        var form_id = req.query.id;
        models.Forms.findByIdAndDelete(form_id).then(function (form) {
            response(res, "Deleted", form, 200);
        }).catch(function (err) {
            response(res, "Invalid id", err, 400);
        });
    });

router.route('/:id')
// getForm: retrieve a form
    .get(function (req, res) {
        models.Forms.findById(req.params.id)
            .then(function (form) {
                if (form) {
                    response(res, 'OK', form, 200);
                } else {
                    response(res, 'The specified resource is not found', req.originalUrl, 404);
                }
            })
            .catch(function (err) {
                response(res, err.message, req.originalUrl, 500);
            })
    });

module.exports = router;

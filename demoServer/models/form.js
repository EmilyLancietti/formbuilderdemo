const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema definition for form object
const formSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Form name is a mandatory field']
    },
    description: {
        type: String,
    },
    form: {
        type: {},
        required: [true, 'Form is a mandatory field']
    },
    version: {
        type: Number,
        default: 1,
        required: [true, 'Form Version is a mandatory field']
    },
    created_at: {
        type: Date,
        default: Date.now
    }
    // TODO: updated at?
}, {
    versionKey: false,
});

// Disallow duplicates (name, version)
formSchema.index({name: 1, version: 1}, {"unique": true});

const Form = mongoose.model('Form', formSchema);

module.exports = {Forms: Form};

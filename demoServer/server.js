const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const mongoose = require('mongoose');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// DB CONNECTION
// FIXME: env var
const url = process.env.DB_URL;
const connect = mongoose.connect(url, {useNewUrlParser: true});
mongoose.set('useCreateIndex', true);

connect.then(function () {
    console.log('Connected correctly to database');
}, function (err) {
    console.log(err);
});

// use it before all route definitions
app.use(cors({origin: 'http://localhost:4200'}));

// APIs
var form = require('./routes/form');
app.use('/form', form);


app.set('port', (process.env.PORT || 8000));

app.listen(app.get('port'), function () {
    console.log('Node app is running on port', app.get('port'));
});

module.exports = app; // for testing

import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Location} from '@angular/common';
import {NavigationExtras, Router} from '@angular/router';


@Component({
  selector: 'app-contact',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {
  forms: Array<object>;
  public previewForm;
  public error = false;
  @ViewChild('preview_form', {static: false}) formPreviewElement?: ElementRef;
  @ViewChild('message', {static: false}) messageElement?: ElementRef;

  constructor(public http: HttpClient, private location: Location, private router: Router) {
  }

  ngOnInit() {
    this.http.get('http://localhost:8000/form').subscribe(
      data => {
        const formList = [];
        JSON.parse(JSON.stringify(data)).data.forEach(form => {
          formList.push(form);
        });
        this.forms = formList;
      },
      error => {
        this.forms = [];
      }
    );
  }

  /**
   * Displays form preview
   * @param form: form's json to render
   */
  preview(form) {
    // Clone form to avoid changes
    this.previewForm =  JSON.parse(JSON.stringify(form));
  }

  edit(form) {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        formData: JSON.stringify(form)
      }
    };
    this.router.navigate(['/builder'], navigationExtras);
  }

  delete(form) {
    this.http.delete(`http://localhost:8000/form?id=${form._id}`).subscribe(
      response => {
        this.messageElement.nativeElement.innerHTML = '';
        this.messageElement.nativeElement.appendChild(document.createTextNode('Form deleted successfully'));
        location.reload();
      },
      error => {
        this.messageElement.nativeElement.innerHTML = '';
        this.error = true;
        this.messageElement.nativeElement.appendChild(document.createTextNode('Invalid request'));
      }
    );
  }

}

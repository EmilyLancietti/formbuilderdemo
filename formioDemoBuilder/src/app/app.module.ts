import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {AppConfig} from './config';
import {FormioAppConfig, FormioModule} from 'angular-formio';
import {FormioResources} from 'angular-formio/resource';
import {FormioAuthService, FormioAuthConfig} from 'angular-formio/auth';
import {BuilderComponent} from './builder/builder.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsComponent} from './forms/forms.component';

/**
 * Import the Custom component CheckMatrix.
 */
import './components/CheckMatrix';
import './components/customComponent';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BuilderComponent,
    FormsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormioModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    FormioResources,
    FormioAuthService,
    {provide: FormioAppConfig, useValue: AppConfig},
    {
      provide: FormioAuthConfig, useValue: {
        login: {
          form: 'user/login'
        },
        register: {
          form: 'user/register'
        }
      }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}



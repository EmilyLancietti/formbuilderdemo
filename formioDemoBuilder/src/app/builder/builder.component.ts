import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './builder.component.html',
  styleUrls: ['./builder.component.scss']
})
export class BuilderComponent implements OnInit {
  formForm;
  public form;
  public error = false;
  @ViewChild('message', {static: false}) messageElement?: ElementRef;
  constructor(public http: HttpClient, private formBuilder: FormBuilder, private router: Router, private activatedRouter: ActivatedRoute) {
    this.form = {
      components: [
        {
          type: 'button',
          label: 'Submit',
          key: 'submit',
          disableOnInvalid: true,
          theme: 'primary',
          input: true,
          tableView: true
        }
      ]
    };
    this.formForm = this.formBuilder.group({
      form_id: '',
      name: '',
      description: '',
      form_json: ''
    });
  }

  ngOnInit() {
    this.activatedRouter.queryParams.subscribe(params => {
      if (params !== {}) {
        const newForm = JSON.parse(params.formData);
        this.formForm = this.formBuilder.group({
          form_id: newForm._id,
          name: newForm.name,
          description: newForm.description,
          form_json: newForm.form
        });
        this.form = JSON.parse(params.formData).form;
      }
    });
  }

  /**
   * Display form json inside builder page
   * @param event: mouse click event
   */
  onChange(event) {
    this.form = event.form;
    this.formForm.controls.form_json.setValue(event.form);
  }

  /**
   * Send form json to server for storage
   * @param data: form data
   */
  submit(data) {
    const payload = {
      id: data.form_id,
      name: data.name,
      description: data.description,
      form: data.form_json
    };
    this.http.post('http://localhost:8000/form', payload).subscribe(
      response => {
        this.router.navigate(['/forms']);
      },
      error => {
        this.messageElement.nativeElement.innerHTML = '';
        this.error = true;
        this.messageElement.nativeElement.appendChild(document.createTextNode('Invalid request'));
      }
    );
  }
}


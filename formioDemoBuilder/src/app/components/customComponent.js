
/**
 * Get the base component class by referencing Formio.Components.components map.
 */
var BaseComponent = Formio.Components.components.base;

/**
 * Create a new CustomSelectBoxesComponent "class" using ES5 class inheritance notation.
 * https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects/Inheritance
 *
 * Here we will derive from the base component which all Form.io form components derive from.
 *
 * @param component
 * @param options
 * @param data
 * @constructor
 */
function CustomSelectBoxesComponent(component, options, data) {
  BaseComponent.prototype.constructor.call(this, component, options, data);
}

// Perform typical ES5 inheritance
CustomSelectBoxesComponent.prototype = Object.create(BaseComponent.prototype);
CustomSelectBoxesComponent.prototype.constructor = CustomSelectBoxesComponent;

/**
 * Define what the default JSON schema for this component is. We will derive from the BaseComponent
 * schema and provide our overrides to that.
 * @return {*}
 */
CustomSelectBoxesComponent.schema = function () {
  return BaseComponent.schema({
    type: 'customselectboxes',
    label: 'CustomSelectBoxes'
  });
};

/**
 * Register this component to the Form Builder by providing the "builderInfo" object.
 *
 * @type {{title: string, group: string, icon: string, weight: number, documentation: string, schema: *}}
 */
CustomSelectBoxesComponent.builderInfo = {
  title: 'Custom Select Boxes',
  group: 'basic',
  icon: 'fa fa-check-square',
  weight: 70,
  documentation: 'http://help.form.io/userguide/#table',
  schema: CustomSelectBoxesComponent.schema()
};

/**
 *  Tell the renderer how to build this component using DOM manipulation.
 */
CustomSelectBoxesComponent.prototype.build = function () {
  this.element = this.ce('div');
  this.createLabel(this.element);

  this.inputs = [];
  this.checks = [];

  var this_component = this;

  // DataSource = url and url exists
  if (this.component.dataSrc === 'url' && this.component.data && this.component.data.url) {
    var url = this.component.data.url;
    // https://vpic.nhtsa.dot.gov/api/vehicles/getmodelsformake/subaru?format=json
    fetch(url)
      .then(function (response) {
        response.json()
          .then(function (result) {
            // selectValues: Data Path           this_component.component.selectValues
            // selectProperty: Value Property    this_component.component.valueProperty
            if (this_component.component.selectValues && this_component.component.valueProperty) {
              result[this_component.component.selectValues].forEach(function (item) {
                build(this_component, item[this_component.component.valueProperty]);
              });
            }
          })
      });
  } else if (this.component.dataSrc === 'values') {
    this.component.data.values.forEach(function (item) {
      build(this_component, item.label);
    })
  } else if (this.component.dataSrc === 'json' && this.component.data['json']) {
    this.component.data['json'].forEach(function (item) {
      var value = this_component.component.valueProperty ? item[this_component.component.valueProperty] : item.value;
      build(this_component, value);
    })
  } else if (this.component.dataSrc === 'custom' || this.component.dataSrc === 'resource') {
    console.log(this.component.data);
    console.log('Not supported');
  } else {
    console.log(this.component.dataSrc);
  }
};

/**
 * Provide the input element information. Because we are using checkboxes, the change event needs to be
 * 'click' instead of the default 'change' from the BaseComponent.
 *
 * @return {{type, component, changeEvent, attr}}
 */
CustomSelectBoxesComponent.prototype.elementInfo = function () {
  const info = BaseComponent.prototype.elementInfo.call(this);
  info.changeEvent = 'click';
  return info;
};

/**
 * Tell the renderer how to "get" a value from this component.
 *
 * @return {Array}
 */
CustomSelectBoxesComponent.prototype.getValue = function () {
  var value = {};
  this.checks.forEach(function (check) {
    value[check.label] = check.value.checked;
  });
  console.log('Value: ', value);
  return value;
};


/**
 * Tell the renderer how to "set" the value of this component.
 *
 * @param value
 * @return {boolean}
 */
CustomSelectBoxesComponent.prototype.setValue = function (value) {
  console.log('value: ', value);
  if (!value) {
    return false;
  }

  this.checks.forEach(function (check) {
    if (!value[check.label]) {
      console.log('false');
      return false;
    } else {
      console.log('true');
      let checked = value[check.label] ? 1 : 0;
      value[check.label].value = checked;
      value[check.label].checked = checked;
    }
  });
};

// Use the table component edit form.
CustomSelectBoxesComponent.editForm = Formio.Components.components.select.editForm;

// Register the component to the Formio.Components registry.
Formio.Components.addComponent('customselectboxes', CustomSelectBoxesComponent);


function build(component, value) {
  var checkbox = component.ce('input', {
    type: 'checkbox',
    value: value,
    hideLabel: false,
  });

  component.checks.push({
    label: checkbox.value,
    value: checkbox
  });

  var span = component.ce('span').appendChild(component.text(" " + value));
  var div = component.ce('div');
  component.addInput(checkbox, div);
  div.appendChild(span);
  component.element.appendChild(div);
}
